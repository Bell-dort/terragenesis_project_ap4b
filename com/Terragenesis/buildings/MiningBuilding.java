package com.Terragenesis.buildings;

import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Mining;
import com.Terragenesis.ressource.Ressource;

public class MiningBuilding extends ExploitationBuilding{
	
	
	public MiningBuilding()
	{
		super();

		this.typeOfBuildingYield=Iron.class.getSimpleName();
		this.ressource=null;
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
	public MiningBuilding(Mining ressource)
	{
		super();

		this.typeOfBuildingYield=ressource.getClass().getSimpleName();
		this.ressource=ressource;
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
	
	
}
