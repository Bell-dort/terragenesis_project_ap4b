package com.Terragenesis.Planete;

public class Atmosphere {
	
	
	protected double diAzote;
	protected double diOxygene;
	protected double dioxydeCarbonne;
	protected double monoxydeDihydrigène; //eau
	
	Atmosphere(){
		this.diAzote=0;
		this.dioxydeCarbonne=0;
		this.diOxygene=0;
		this.monoxydeDihydrigène=0;
	}

	public void genereratingAtmosphere() {
		
		this.dioxydeCarbonne=Math.random()*0.01;
		this.monoxydeDihydrigène= (0.1-this.dioxydeCarbonne);
		this.diOxygene=Math.random()*100;
		this.diAzote= (99-this.diOxygene);
	}


}
