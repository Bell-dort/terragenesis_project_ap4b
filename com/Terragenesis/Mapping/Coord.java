package com.Terragenesis.Mapping;

public class Coord {
	protected int x,y;

	public Coord() {
		this.x=0;
		this.y=0;
	}
	public Coord(int x, int y) {
		this.x=x;
		this.y=y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	@Override
	public String toString() {
		return "Coord [x=" + x + ", y=" + y + "]";
	}
	
	
	

}
