package com.Terragenesis.players;

public class Population {
	
 	protected double happiness;
	protected double foodconsumption;
	protected double drinkingWaterConsumption;
	protected int numberPopulation;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Population() {
		this.drinkingWaterConsumption=10;
		this.foodconsumption=10;
		this.numberPopulation=1000;
		this.happiness=10;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Population(double happiness, double foodconsumption, double drinkingWaterConsumption ,int numberPopulation) {
		this.drinkingWaterConsumption=drinkingWaterConsumption;
		this.foodconsumption=foodconsumption;
		this.numberPopulation=numberPopulation;
		this.happiness=happiness;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
	public void evolvePopulation() {
		
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public double getHappiness      		 () 					  		   {return happiness;}
	public void   setHappiness      		 (double happiness) 	  		   {this.happiness = happiness;}
	public double getFoodconsumption		 () 					  		   {return foodconsumption;}
	public void   setFoodconsumption         (double foodconsumption) 		   {this.foodconsumption = foodconsumption;}
	public double getDrinkingWaterConsumption() 					  		   {return drinkingWaterConsumption;}
	public void   setDrinkingWaterConsumption(double drinkingWaterConsumption) {this.drinkingWaterConsumption = drinkingWaterConsumption;}
	public int    getNumberPopulation		 () 							   {return numberPopulation;}
	public void   setNumberPopulation		 (int numberPopulation) 		   { this.numberPopulation = numberPopulation;}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return "Population [happiness=" + happiness + ", foodconsumption=" + foodconsumption
				+ ", drinkingWaterConsumption=" + drinkingWaterConsumption + ", numberPopulation=" + numberPopulation
				+ "]";
	}
	
	

}
