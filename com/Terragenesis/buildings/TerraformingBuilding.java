package com.Terragenesis.buildings;
import java.util.ArrayList;

import com.Terragenesis.buildings.terraform.*;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;

public abstract class TerraformingBuilding extends Building {
	

	Terraform level = new TerraLevel_I();
	
	protected TerraformingBuilding() {
		
		this.neededRessource=new ArrayList<Ressource>();
		this.neededRessource.add(new Iron(400));
		this.neededRessource.add(new Sand(1000));
	}
	
	protected TerraformingBuilding(ArrayList<? extends Ressource > needed) {
		
		this.neededRessource=new ArrayList<Ressource>();
		this.neededRessource.addAll(needed);
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void levelUp() {
		if( this.level.getClass().getName().equals( new TerraLevel_III().getClass().getName() ) ) { this.level= new TerraLevel_IV(); }
		if( this.level.getClass().getName().equals( new TerraLevel_II().getClass().getName()  ) ) { this.level= new TerraLevel_III(); }
		if( this.level.getClass().getName().equals( new TerraLevel_I().getClass().getName()   ) ) { this.level= new TerraLevel_II(); }

		this.costLevelUp=level.getCostLevelUp();

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void LevelDown() {
		if( this.level.getClass().getName().equals( new TerraLevel_II().getClass().getName()   ) ) { this.level= new TerraLevel_I(); }
		if( this.level.getClass().getName().equals( new TerraLevel_III().getClass().getName()  ) ) { this.level= new TerraLevel_II(); }
		if( this.level.getClass().getName().equals( new TerraLevel_IV().getClass().getName()   ) ) { this.level= new TerraLevel_III(); }
		
		this.costLevelUp=level.getCostLevelUp();

	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public double terraform() {
		return this.level.terraform();
	}
	
	
}
