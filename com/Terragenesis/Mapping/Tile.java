package com.Terragenesis.Mapping;
import java.util.ArrayList;
import java.util.HashMap;

import com.Terragenesis.buildings.*;
import com.Terragenesis.ressource.*;
import com.Terragenesis.players.*;
import com.Terragenesis.Interface.Cell;

public class Tile extends Cell{

	public static int TotalAllocatePopulation=0;
	
	
	protected Building construction;
	protected ArrayList<Ressource> ressource;
	protected ArrayList<TileEffect> effect;
	protected Coord coord;	
	
	public Tile() {
		this.construction= null;
		this.ressource= new ArrayList<Ressource>();
		this.effect= new ArrayList<TileEffect>();
	}
	
	

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public Tile(int x, int y) {
		this.construction= null;
		this.ressource= new ArrayList<Ressource>();
		this.effect= new ArrayList<TileEffect>();
		this.coord= new Coord(x,y);
	}
	
	

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public Tile( Building buildings, ArrayList<Ressource> ressources ) { //check if polymorhism work and you can call son class
		this.construction = buildings;
		this.ressource    = ressources;
		this.effect= new ArrayList<TileEffect>();

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public Tile( Building buildings,Ressource ressources ) { 
		
		this.construction = buildings;
		this.effect=null;
		this.ressource    = new ArrayList<Ressource>();
		this.ressource.add(ressources);
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public boolean build  ( Building bat, Stat stat) {	
		
		if( !(this.isBuildingHere() ) && this.CheckCanConstruct(bat, stat) ) { 
			
			this.construction=bat;
			stat.addBuidling(bat);
			stat.constructBuilding(bat);
			
			return true;
			} 
		return false;
		}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void destroy(){ 
		this.construction=null; 
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public boolean isBuildingHere	(){
		
		return (this.construction!=null);
		}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public boolean CheckCanConstruct	(Building bat, Stat stat) { 
		
		int i=0,j=-1;
		double batRessource;
		boolean stop=false;
	
		for(i=0;i<bat.getNeededRessource().size();i++) {
			
			batRessource=bat.getNeededRessource().get(i).getNumber();

			 do{
				 j++;
				stop = bat.getNeededRessource().get(i).getClass().getSimpleName().
			    equals( stat.getRessourceStat().get(j).getClass().getSimpleName() );
				
			}while(  j<stat.getRessourceStat().size() && !stop );
			

				if( batRessource > stat.getRessourceStat().get(j).getNumber() ) {
					return false;
				}
				
		}
		
	 return true;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public boolean CheckCanLevelUp  (Stat stat) {	
		if(this.construction.getActualLevel()>=4)
			return false;
		
		return (stat.getScience()>= this.construction.getCostLevelUp()); 
		
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void addRessource(ArrayList<?extends Ressource> list) {

		this.ressource.addAll(list);
		
	}
	public void addRessource( Ressource build) {
		this.ressource.add(build);
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void doActionBuilding(Stat stat) {
		if( this.construction.isConstructing())
			return;

		if( this.construction.getClass().getSuperclass().getSimpleName().equals( ExploitationBuilding.class.getSimpleName() ) ) { //check if it's a explot building
			this.construction.doBuildingAction(stat, this);
		}

		if( this.construction.getClass().getSuperclass().getSimpleName().equals( CivilBuilding.class.getSimpleName() ) ) {
			this.construction.doBuildingAction(stat,this);	
		}
	
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void farmeffect() {
		if( this.effect==null) {
			
			this.effect=new ArrayList<TileEffect>();
		}

		this.effect.add( new TileEffect( new AgriculturalBuilding(), 2d ) );
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public boolean allocatePop(int pop,Stat stat) {
		
		if(!this.isBuildingHere())
			return false;
		
		if( ( this.TotalAllocatePopulation+pop ) < stat.getPop().getNumberPopulation()) {
			//System.out.printf("\n --pop=%d---------%d---\n",pop, this.construction.getNumberPopulation());
			this.TotalAllocatePopulation+=pop;
			this.construction.setNumberPopulation(pop);
		//	System.out.printf("\n --pop=%d---------%d---\n",pop, this.construction.getNumberPopulation());

			return true;
		}
		return false;
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
		public int getRessourceIndex(Ressource r) {
			
			int i=-1;
			boolean stop=false;
			
			do{
				i++;
				if( r.getClass().getSimpleName().equals( this.ressource.get(i).getClass().getSimpleName() ) ){
					stop=true;
				}
			}while( i<this.ressource.size() && !stop );
			
			return i;
		}
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
	@Override
	public String toString() {
		return "Tile [construction=" + construction + ", ressource=" + ressource + "]";
	}

	public static int getTotalAllocatePopulation() {
		return TotalAllocatePopulation;
	}

	public static void setTotalAllocatePopulation(int totalAllocatePopulation) {
		TotalAllocatePopulation = totalAllocatePopulation;
	}

	public Building getConstruction() {
		return construction;
	}

	public void setConstruction(Building construction) {
		this.construction = construction;
	}

	public ArrayList<Ressource> getRessource() {
		return ressource;
	}

	public void setRessource(ArrayList<Ressource> ressource) {
		this.ressource = ressource;
	}

	public ArrayList<TileEffect> getEffect() {
		return effect;
	}

	public void setEffect(ArrayList<TileEffect> effect) {
		this.effect = effect;
	}




	
	
	
}
