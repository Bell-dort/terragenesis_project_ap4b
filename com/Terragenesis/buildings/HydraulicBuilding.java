package com.Terragenesis.buildings;

import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.DrinkingWater;
import com.Terragenesis.ressource.Hydraulic;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;

public class HydraulicBuilding extends ExploitationBuilding{
	
	public HydraulicBuilding()
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.ressource=null;
		this.typeOfBuildingYield=DrinkingWater.class.getSimpleName();


	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
	public HydraulicBuilding(Hydraulic ressource)
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.ressource=ressource;
		this.typeOfBuildingYield=ressource.getClass().getSimpleName();

		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




}
