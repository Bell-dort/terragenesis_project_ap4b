package com.Terragenesis.buildings;

import java.util.ArrayList;

import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Ressource;

public abstract class Building {
	
	protected String typeOfBuildingYield;
	protected int numberPopulation;
	protected int constructionTime;
	protected int remainingTime;
	protected double costLevelUp;
	
	protected ArrayList<Ressource> neededRessource;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public abstract void levelUp();
	public abstract void LevelDown();
	public abstract void doBuildingAction(Stat stat, Tile tile );
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public boolean isConstructing() {
		if (this.remainingTime==0)
			return false;
		
		return true;	
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void construct() {
	
		if(this.remainingTime<=0)
			return;
		
		this.remainingTime--;
	}
	

	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	 

		public int getRessourceIndex(Ressource r) {
			
			int i=-1;
			boolean stop=false;
			
			do{
				i++;
				if( r.getClass().getSimpleName().equals( this.neededRessource.get(i).getClass().getSimpleName() ) ){
					stop=true;
				}
			}while( i<this.neededRessource.size() && !stop );
			
			return i;
		}
		
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	public double getCostLevelUp() 			 				{ return this.costLevelUp; 	   	 				}
	public int    getNumberPopulation() 		   			{ return numberPopulation;  					}
	public void   setNumberPopulation(int numberPopulation) {   this.numberPopulation = numberPopulation;  	}
	public int    getConstructionTime() 					{ return constructionTime;						}
	public void   setConstructionTime(int constructionTime) { 	this.constructionTime = constructionTime;	}
	public int    getRemainigTime()						    { return remainingTime;							}
	public void   setRemainigTime(int remainigTime)		    {	this.remainingTime = remainigTime;			}
	public String getTypeOfBuildingYield() 					{ return this.typeOfBuildingYield; 				}
	public ArrayList<Ressource> getNeededRessource() 		{ return neededRessource;						}
	
	public void setNeededRessource(ArrayList<Ressource> neededRessource) {this.neededRessource = neededRessource;}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

	

	
	
	
}
