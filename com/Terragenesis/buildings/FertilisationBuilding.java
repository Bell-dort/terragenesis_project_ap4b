package com.Terragenesis.buildings;

import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Ressource;

public class FertilisationBuilding extends TerraformingBuilding{

	 public FertilisationBuilding()
	{
		this.costIron=400;
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
	}
	
	
}
