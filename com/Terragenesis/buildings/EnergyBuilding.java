package com.Terragenesis.buildings;

import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Energy;
import com.Terragenesis.ressource.EnergyPower;
import com.Terragenesis.ressource.Mining;
import com.Terragenesis.ressource.Ressource;

public class EnergyBuilding extends ExploitationBuilding{
	
	public EnergyBuilding()
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.ressource=null;
		this.typeOfBuildingYield=Energy.class.getSimpleName();

	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public EnergyBuilding( EnergyPower ressource)
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.ressource=ressource;
		this.typeOfBuildingYield=ressource.getClass().getSimpleName();

		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	


}
