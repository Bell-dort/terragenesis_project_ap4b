package com.Terragenesis.buildings;
import java.util.ArrayList;

import com.Terragenesis.buildings.civil.*;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;

public abstract class CivilBuilding extends Building{
	
	Civil level = new CivilLevel_I();
	
	protected CivilBuilding() {
		
		this.neededRessource=new ArrayList<Ressource>();
		this.neededRessource.add(new Iron(400));
		this.neededRessource.add(new Sand(1000));
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	protected CivilBuilding(ArrayList<? extends Ressource > needed) {
		
		this.neededRessource=new ArrayList<Ressource>();
		this.neededRessource.addAll(needed);
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void levelUp() {
		if( this.level.getClass().getName().equals( new CivilLevel_III().getClass().getName() ) ) { this.level= new CivilLevel_IV();  }
		if( this.level.getClass().getName().equals( new CivilLevel_II() .getClass().getName() ) ) { this.level= new CivilLevel_III(); }
		if( this.level.getClass().getName().equals( new CivilLevel_I()  .getClass().getName() ) ) { this.level= new CivilLevel_II();  }

		this.costLevelUp=level.getCostLevelUp();


	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void LevelDown() {
		if( this.level.getClass().getName().equals( new CivilLevel_II().getClass() .getName()  ) ) { this.level= new CivilLevel_I(); }
		if( this.level.getClass().getName().equals( new CivilLevel_III().getClass().getName()  ) ) { this.level= new CivilLevel_II(); }
		if( this.level.getClass().getName().equals( new CivilLevel_IV().getClass() .getName()  ) ) { this.level= new CivilLevel_III(); }
		
		this.costLevelUp=level.getCostLevelUp();

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public double research() {
		return this.level.research();
	}


}
