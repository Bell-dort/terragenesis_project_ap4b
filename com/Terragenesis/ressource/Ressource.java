package com.Terragenesis.ressource;

public abstract class Ressource {
	
	protected double number;
	
	protected void generateQuantity(double max) {
		
		this.number= Math.random()*(max);
	}
	

	public double getNumber() 			 { return number; }
	public void setNumber(double number) {
		
		this.number = number;
		if(this.number<0)
			this.number=0;
	}
	
	public boolean isEnough(double num) {
		return (this.number-num)>=0;
	}
	
	public void extract(double quantity) {
		this.number-=quantity;	
	}
	
	public void soustract(double quantity) {
		if( this.number-quantity>0 ) {
			this.number-=quantity;
		}
	}
	


	@Override
	public String toString() {
		return "Ressource [number=" + number + "]";
	}
	
	
}
