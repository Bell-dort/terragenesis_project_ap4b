package com.Terragenesis.buildings.Extraction;

public class ExtractionLevel_IV implements Extraction{
	
	public double extract() {
		return 4*Extraction.num;
	}
	public double getCostLevelUp() {
		return 4*Extraction.costLevel;
	}
	public double getPop() {
		return 4*Extraction.nbPop;
	}
}
