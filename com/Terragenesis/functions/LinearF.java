package com.Terragenesis.functions;

public class LinearF {
	
	protected double coef, ordonnee;
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public  LinearF() {
		this.coef=1;
		this.ordonnee=0;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public  LinearF(double coef, double ordonnee) {
		this.coef=coef;
		this.ordonnee=ordonnee;
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public LinearF(int x, int y) {
		this.randLinear((double)x, (double)y);
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public double calculate(double x) { return this.coef*x+this.ordonnee; }

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public boolean isFunctionBetween(double x, double y, double a , double b) {
		
		if( this.calculate(x)==y && this.calculate(a)==b)
			return true;
		
		boolean Point_1_Isupper = ( this.calculate(x)<y );
		boolean Point_2_Isupper = ( this.calculate(a)<b );
		
		
		return (Point_1_Isupper != Point_2_Isupper) ;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void randLinear(double x,double y) {
		int i;
		double denominator=1;
		double coef=1;
		double m=0;
		double num;
		m=Ftool.randDouble(-2*y,2*y);
		
		if(0<=m && m<=y){	
			coef=Ftool.randDouble(-y, y);
		
		} else {
				num=Ftool.randDouble(1, x);
				coef=(1-m)/num;
		 	   }	
		
		
		this.coef=coef;
		this.ordonnee=m;
			
	
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public double getCoef    () 				{ return coef;				}
	public void   setCoef    (double coef) 		{ this.coef = coef;			}
	public double getOrdonnee() 				{ return ordonnee;			}
	public void   setOrdonnee(double ordonnee) 	{ this.ordonnee = ordonnee;	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return "LinearF [coef=  " + (int)coef + ", ordonnee=  " + (int)ordonnee + "]";
	}
	

}
