package com.Terragenesis.functions;


public class Ftool {
	
	public static double linearF(double x , double coef, double m) {
		return coef*x+m;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public static double squareF(double x ,double a,double b , double m) {
		return a*(x*x)+b*x+m;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
	public static double cubeF(double x ,double a, double b,double c , double m) {
		return a*(x*x*x)+b*(x*x)+c*x+m;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public static int randInt (int min, int max) {
		return (int) (Math.random() *(max-min+1))+min ;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public static double randDouble (double min, double max) {
		return (Math.random() *(max-min+1))+min;
	}
	
	static public double gauss(double x, double mu, double sigma) {
		return ( 1/( sigma*Math.sqrt(2*Math.PI) )* Math.exp( (-1/2)*Math.pow( (x-mu)/sigma ,2) ) ); 
	}
	 
	
	

}
