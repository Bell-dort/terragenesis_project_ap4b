package com.Terragenesis.buildings.Extraction;

public class ExtractionLevel_III implements Extraction{
	
	public double extract() {
		return 3*Extraction.num;
	}
	public double getCostLevelUp() {
		return 3*Extraction.costLevel;
	}
	public double getPop() {
		return 3*Extraction.nbPop;
	}

}
