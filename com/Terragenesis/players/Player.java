package com.Terragenesis.players;

public class Player {
	
	protected int maxActionPoint;
	protected int actionPoint;
	protected Stat playerStat;
	
	
	
	
	public Player() {
		super();
		this.maxActionPoint = 0;
		this.actionPoint = 0;
		this.playerStat = new Stat();
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Player(int maxActionPoint, int actionPoint, Stat playerStat) {
		super();
		this.maxActionPoint = maxActionPoint;
		this.actionPoint = actionPoint;
		this.playerStat = playerStat;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public int  getMaxActionPoint() 				  { return maxActionPoint;}
	public void setMaxActionPoint(int maxActionPoint) { this.maxActionPoint = maxActionPoint; }
	public int  getActionPoint   ()					  { return actionPoint; }
	public void setActionPoint   (int actionPoint) 	  {	this.actionPoint = actionPoint; }
	
	
	

}
