package com.Terragenesis.Mapping;

import java.util.ArrayList;

import com.Terragenesis.functions.Ftool;
import com.Terragenesis.functions.LinearF;
import java.awt.color.*;
import java.awt.*;

public class Area {
	
	protected int x,y;
	protected int matrix [][];
	protected int numberFunct;
	protected int numbArea=0;
	ArrayList<LinearF> function= new ArrayList();

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

	public void creatArea() {
		int i=0, j=0;
		int count=0;
		
		for(i=0;i<this.x;i++) {
			for(j=0;j<this.y;j++) {
				if(this.matrix[i][j]==0) {
					this.testSide(i, j, count);
					count++;
					this.numbArea++;
				}
			}
		}
	
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	private void testSide(int x, int y,int count) {
		int i,j;
		int a,b;
		for(i=-1;i<=1;i++) {
			for(j=-1;j<=1;j++){
				a=x+i;
				b=y+j;
				if( (a<this.x) && a>=0 && (b<this.y) && b>=0 ) {

						if( !(this.isFunctionsBetween(x,y,a,b)) && this.matrix[a][b]!=count ) {

								this.matrix[a][b]=count;
								this.testSide(a, b, count);
						} 
				}
			}
		}
			
			
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public boolean isFunctionsBetween(int x, int y, int a , int b) {
		int i;
		for(i=0; i<this.function.size();i++) {
			
			if( this.function.get(i).isFunctionBetween(x, y, a, b) ) {
				return true;
			}
		}
		return false;
	}
	

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Area() {
		this.x=10;
		this.y=10;
		this.matrix=new int[this.x][this.y] ;
		this.numberFunct=5;
		int i,j;
		
		for(i=0; i<this.x;i++) {
			for(j=0;j<this.y;j++) {
				this.matrix[i][j]=0;
			}
		}
		for(i=0;i<this.numberFunct;i++) {
			function.add(new LinearF(this.x,this.y)  ) ;																	//creating function to cross the map and so draw Area		
		}
		this.creatArea();

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Area(int x ,int y, int number) {
		this.x=x;
		this.y=y;
		this.matrix=new int[this.x][this.y] ;
		this.numberFunct=number;

		int i,j;
		
		for(i=0; i<this.x;i++) {
			for(j=0;j<this.y;j++) {
				this.matrix[i][j]=0;
			}
		}
		
		
		for(i=0;i<this.numberFunct;i++) {
			
			function.add(new LinearF(this.x,this.y)  ) ;																	//creating function to cross the map and so draw Area		
		}
		this.creatArea();
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void showGrid() {
		int i,j;
		for(j=this.y-1;j>=0;j--) {
			
			for(i=0;i<this.x;i++) {
		
				System.out.printf(" %d ", this.matrix[i][j]);
			}
			
			System.out.printf("\n");
		}
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public int 				  getX() 	  	{ return x; 	   }
	public void 			  setX(int x) 	{ this.x = x;	   }
	public int 				  getY() 	  	{ return y;		   }
	public void 			  setY(int y)	{ this.y = y;	   }
	public int[][] 			  getMatrix  ()	{ return matrix;   }
	public int				  getNumbArea() { return numbArea; }
	public ArrayList<LinearF> getFunction() { return function; }
	public void 			  setFunction(ArrayList<LinearF> function) {this.function = function;}
	
	
	

}
