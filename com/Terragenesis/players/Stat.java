package com.Terragenesis.players;

import java.util.ArrayList;
import java.util.HashMap;

import com.Terragenesis.ressource.*;
import com.Terragenesis.Mapping.Coord;
import com.Terragenesis.buildings.*;


public class Stat {

	protected ArrayList<Building>  buildingStat;
	protected ArrayList<Ressource> ressourceStat;
	protected ArrayList<Coord> coord= new ArrayList<Coord>();
	
	public ArrayList<Coord> getCoord() {
		return coord;
	}

	public void setCoord(ArrayList<Coord> coord) {
		this.coord = coord;
	}
	protected Population pop;
	
	protected double culture;
	protected double science;
	protected double TotalFood;
	protected double energy;
	
	
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public Stat() {
			super();
			this.buildingStat = new ArrayList<Building>();
			this.ressourceStat= new ArrayList<Ressource>();
			
			this.ressourceStat.add(new Iron());
			this.ressourceStat.add(new Sand());
			this.ressourceStat.add(new Energy());
			this.ressourceStat.add(new Wheat());
			this.ressourceStat.add(new DrinkingWater());

			
			this.pop = new Population();
			this.culture = 0;
			this.science = 0;
			this.TotalFood=0;
		}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public Stat(ArrayList<Ressource> ressource) {
			super();
			this.ressourceStat=ressource;

			this.buildingStat = new ArrayList<Building>();
			
			this.pop = new Population();
			this.culture = 0;
			this.science = 0;
			this.TotalFood=0;
		}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Stat(ArrayList<Building> buildingStat, Population pop, ArrayList<Ressource> ressource, double culture, double science, double food) {
		super();
		this.ressourceStat= ressource;
		this.buildingStat = buildingStat;
		this.pop = pop;
		this.culture = culture;
		this.science = science;
		this.TotalFood=food;

		
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void addBuidling(ArrayList <? extends Building > list) {
		this.buildingStat.addAll(list);
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void addBuidling(Building build) {
		this.buildingStat.add(build);
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public  ArrayList<Building> getTypeOfBuilding(String nameBuilding) { //get all the building of a type
		
		ArrayList<Building> list=new ArrayList<Building>();
		int i;
		
		for(i=0; i<this.buildingStat.size(); i++)
		{
			if(this.buildingStat.get(i).getClass().getSimpleName().equals(nameBuilding))
				list.add(this.buildingStat.get(i));
		}
		return list;
	}
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public int getRessourceIndex(Ressource r) {
		
		int i=-1;
		boolean stop=false;
		
		do{
			i++;
			if( r.getClass().getSimpleName().equals( this.ressourceStat.get(i).getClass().getSimpleName() ) ){
				stop=true;
			}
		}while( i<this.ressourceStat.size() && !stop );
		
		return i;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void constructBuilding(Building b) {
		double get;
		for(int i=0; i<b.getNeededRessource().size();i++) {
			
			this.ressourceStat.get( this.getRessourceIndex(b.getNeededRessource().get(i)) ).soustract(b.getNeededRessource().get(i).getNumber());	
		}
	
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
	public double getCulture() 			 	 { return culture;		   }
	public void   setCulture(double culture) { this.culture = culture; }
	public double getScience() 				 { return science;   	   }
	public void   setScience(double science) { this.science = science; }
	public double getTotalFood() 			 { return this.TotalFood;  }
	public void   setTotalFood(double food)	 { this.TotalFood=food;    }
	public Population getPop() 				 { return pop;			   }
	public void   setPop(Population pop) 	 { this.pop = pop;  	   }
	public double getEnergy() 				 { return energy;		   }
	public void   setEnergy(double energy) 	 { this.energy = energy;   }

	public ArrayList<Ressource> getRessourceStat() 									 { return ressourceStat;				}
	public void 				setRessourceStat(ArrayList<Ressource> ressourceStat) { this.ressourceStat = ressourceStat;	}
	public ArrayList<Building>  getBuildingStat() 									 { return buildingStat;					}
	public void 				setBuildingStat(ArrayList<Building> buildingStat) 	 { this.buildingStat = buildingStat;	}
	
	

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	
	
	
}
