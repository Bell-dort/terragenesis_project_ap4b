import java.util.ArrayList;
import java.util.HashMap;

import com.Terragenesis.Mapping.Area;
import com.Terragenesis.Mapping.Map;
import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.buildings.*;
import com.Terragenesis.functions.Ftool;
import com.Terragenesis.functions.LinearF;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.*;

public class Terragenesis {

	public static void main(String[] args) {
		
		Map a =new Map(5,5);
		a.showMap();		System.out.printf("\n");
		System.out.printf("\n");
		System.out.printf("\n");

		
		ArrayList<Ressource> list= new ArrayList<Ressource>();

		list.add(new Iron		  (9000)				);
		list.add(new Sand 		  (9000)				);
		list.add(new Energy		  (9000)				);
		list.add(new Wheat		  (9000)				);
		list.add(new DrinkingWater(9000)				);
		
		Stat stat= new Stat(list);
		stat.setScience(1000);
		
		System.out.print(stat.getRessourceStat().toString());
		System.out.printf("\n");


		a.build(0, 0, new AgriculturalBuilding(new Wheat()), stat);
		a.allocationPop(0, 1, stat, 50);
		a.levelUpBuilding(stat, 0, 0);
		//a.build(0, 1, new MiningBuilding(new Iron()), stat);
		System.out.print(a.allocationPop(0, 1, stat, 50));
		
		a.build(2, 2, new ScientificBuilding(), stat);
		

		
		a.harvestBuilding(stat);
		System.out.printf("\n");
		System.out.print(stat.getScience());
		System.out.printf("\n");

		System.out.print(stat.getRessourceStat().toString());

		
	}
	

}
