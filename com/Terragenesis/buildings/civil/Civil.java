package com.Terragenesis.buildings.civil;

import com.Terragenesis.buildings.Extraction.Extraction;

public interface Civil {
	 
	double num=20;
	double costLevel=50;
	double nbPop=100;
	
	public double research();
	public double getCostLevelUp();
	public double getPop();
	
}
