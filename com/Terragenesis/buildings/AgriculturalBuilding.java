package com.Terragenesis.buildings;

import java.util.ArrayList;

import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Foodstuff;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Mining;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;
import com.Terragenesis.ressource.Wheat;

public class AgriculturalBuilding extends ExploitationBuilding{
	
	
	public AgriculturalBuilding()
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.ressource=null;
		this.typeOfBuildingYield=Wheat.class.getSimpleName();
		

	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public AgriculturalBuilding(Foodstuff ressource)
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.ressource=ressource;
		this.typeOfBuildingYield=ressource.getClass().getSimpleName();

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
