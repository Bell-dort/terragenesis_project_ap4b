package com.Terragenesis.buildings;

import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Ressource;

public class AtmosphereBuilding extends TerraformingBuilding {
	 public AtmosphereBuilding()
		{
			this.costIron=400;
			this.numberPopulation=0;
			this.constructionTime=3;
			this.remainingTime=this.constructionTime;
			this.costLevelUp=this.level.getCostLevelUp();
		}

	
}
