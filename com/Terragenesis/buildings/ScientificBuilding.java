package com.Terragenesis.buildings;

import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.buildings.civil.Civil;
import com.Terragenesis.buildings.civil.CivilLevel_I;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Ressource;

public class ScientificBuilding extends CivilBuilding  {
	
	public ScientificBuilding()
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.level=new CivilLevel_I();

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public ScientificBuilding(Civil level)
	{
		super();
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.level=level;

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void doBuildingAction(Stat stat, Tile tile) {
		stat.setScience(stat.getScience() + this.level.research() );
		
		
	}
	

}
