package com.Terragenesis.Mapping;

import java.util.ArrayList;

import com.Terragenesis.ressource.DrinkingWater;
import com.Terragenesis.ressource.Energy;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;
import com.Terragenesis.ressource.Wheat;
import com.Terragenesis.buildings.Building;
import com.Terragenesis.functions.*;
import com.Terragenesis.players.Stat;

public class Map {

	protected ArrayList<ArrayList<Tile>> map =new ArrayList<ArrayList<Tile>>();
	protected ArrayList<ArrayList<Tile>> Area=new ArrayList<ArrayList<Tile>>();
	protected ArrayList<ArrayList<Tile>> Biome= new ArrayList<ArrayList<Tile>>();
	protected int x ,y;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
	public enum BIOME
	{	    /* Locatation /\ Iron /\ Sand /\ Energy /\  Wheat /\ DrinkingWater /\ effect */
		Desert (   0,         3,      1,       1,         0,           0,			0		), 
		Toundra(   1,         0,      0,       0,         0,           3,			0		), 
		plain  (   2,         1,      0,       0,         3,           1,			1		), 
		Sea    (   3,         0,      3,       1,         0,           1,			0		),;
		
		private int loca;
		private int Iron;
		private int Sand;
		private int Energy;
		private int Wheat;
		private int DrinkingWater;
		private int effect;
		public static  int size=4;
		public static   int nbparam=5;

		
		BIOME(int loc, int iron, int Sand, int Energy, int Wheat, int DrinkingWater, int effect){
			this.loca		  =loc;
			this.Iron		  =iron;
			this.Sand 		  =Sand;
			this.Energy		  =Energy;
			this.Wheat        =Wheat;
			this.DrinkingWater=DrinkingWater;
			this.effect       =effect;
			
		}
		public int getloca  () { return this.loca;  }
		public int getIron  () { return this.Iron;  }
		public int getSand  () { return this.Sand;  }
		public int getEnergy() { return this.Energy;  }
		public int getWheat () { return this.Wheat; }
		public int getEffect() { return this.effect;  }
		public int getDrinkingWater() { return this.DrinkingWater;	}



		
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public Map() {
		this.x=10;
		this.y=10;
		int i=0,j=0;
		for(i=0;i<x;i++) {
			ArrayList<Tile> row=new ArrayList<Tile>();
			for(j=0;j<this.y;j++) {
				row.add( new Tile(i,j) );
				
			}
			this.map.add(row);
		}
		this.creatArea();
		this.creatBiom();
		

	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public Map(int x, int y) {
		this.x=x;
		this.y=y;
		int i=0,j=0;
		
		for(i=0;i<this.x;i++) {
			
			ArrayList<Tile> row=new ArrayList<Tile>();
			
			for(j=0;j<this.y;j++) {
				row.add(new Tile(i,j));

			}
			this.map.add(row);
		}
	
		this.creatArea();
		this.creatBiom();
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	private void creatArea() {
		
		int i,j;
		Area area =new Area(this.x,this.y, (int)Math.sqrt(this.x)*2 );
		for(i=0;i<area.numbArea;i++) {
			this.Area.add( new ArrayList<Tile>() );
		}

		for(i=0;i<this.x;i++) {
			for(j=0;j<this.y;j++) {
				
				this.Area.get( area.matrix[i][j] ).add( this.map.get(i).get(j) );
				
			}
		}				
	
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void creatBiom() {
		
		int i=0,j=0;
		int range=0;
		
		for(i=0;i<BIOME.size;i++)
			this.Biome.add(new ArrayList<Tile>());
		
		for(i=0;i<this.Area.size();i++) {
			
			range=Ftool.randInt(0, (BIOME.size-1));

			for(j=0; j<this.Area.get(i).size();j++) {

				this.Biome.get(range).add( this.Area.get(i).get(j) );				
			}			
		}
		this.biomApportionment();		
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	private void biomApportionment() {

		int max=10000;
		Integer div[]= new Integer[BIOME.nbparam];
		int i=0,j=0;
		
		BIOME arr[]=BIOME.values();

		for(BIOME b :arr) {

			for(j=0; j<this.Biome.get(b.getloca()).size();j++) {
				

				for(int k=0;k<BIOME.nbparam;k++) {	
					div[k]=(int)max/Ftool.randInt(1,50);
				}

				ArrayList<Ressource> list= new ArrayList<Ressource>();

				list.add(new Iron		  (b.getIron()	  	   *div[0])				);
				list.add(new Sand 		  (b.getSand()	  	   *div[1])				);
				list.add(new Energy		  (b.getEnergy()  	   *div[2])				);
				list.add(new Wheat		  (b.getWheat()	  	   *div[3])				);
				list.add(new DrinkingWater(b.getDrinkingWater()*div[4])				);
				
				this.Biome.get(b.getloca()).get(j).addRessource(list);
				this.Biome.get(b.getloca()).get(j).setColor(b.getloca());	//change la couleur de base en fonction du Biome
				this.Biome.get(b.getloca()).get(j).setBiome(b.name());
				
			}

		}
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void build(int x, int y, Building built, Stat stat) {
		
		if( this.map.get(x).get(y).build(built, stat) )
			stat.getCoord().add(new Coord(x,y));
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public void harvestBuilding(Stat stat) {
		
		for(int i=0;i<stat.getCoord().size();i++) {
			System.out.printf("\n --x=%d---------y=%d---\n",stat.getCoord().get(i).getX(),stat.getCoord().get(i).getY());
			this.map.get(stat.getCoord().get(i).getX() )
					.get(stat.getCoord().get(i).getY() ).doActionBuilding(stat) ;
		}
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public void levelUpBuilding(Stat stat, int x, int y) {
		
		if( this.map.get(x).get(y).CheckCanLevelUp( stat) ) {
			
			this.map.get(x).get(y).construction.levelUp();
			stat.setScience( stat.getScience()-this.map.get(x).get(y).construction.getCostLevelUp() );
		}
	}
	
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
	public void showMap() {
			
			for(int j=0;j<this.y-1;j++) {
				
				for(int i=0 ;i<this.x-1;i++) {
//					System.out.print(this.map.get(0).get(0).getRessource().get(0).toString() );
				System.out.print(this.map.get(i).get(j).getRessource().toString());
				}
				System.out.printf("\n");
			}
		}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public boolean allocationPop(int x, int y, Stat stat, int pop) {
		
		return this.map.get(x).get(y).allocatePop(pop, stat);
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public ArrayList<ArrayList<Tile>> getMap  () { return map; }
	public void 					  setMap  (ArrayList<ArrayList<Tile>> map) 	 { this.map = map;  }
	public ArrayList<ArrayList<Tile>> getArea () 								 { return Area; 	}
	public void 					  setArea (ArrayList<ArrayList<Tile>> area)  { Area = area; 	}
	public ArrayList<ArrayList<Tile>> getBiome() 								 { return Biome; 	}
	public void 					  setBiome(ArrayList<ArrayList<Tile>> biome) { Biome = biome; 	}
	public int 						  getX    () 								 {	return x; 		}
	public void 					  setX    (int x) 							 { this.x = x; 		}
	public int						  getY	  () 								 {	return y; 		}
	public void 					  setY	  (int y) 							 {this.y = y; 		}
	
	
	

}
